#!/usr/bin/env python

import requests, re, urlparse

def request(url):
    try:
        url = "http://www." + url
        response = requests.get(url)
        return response
    except requests.exceptions.ConnectionError:
        pass

def get_links(response):
    try:
        links = re.findall('(?:href=")(.*?)"', response.content)
        return links
    except AttributeError:
        pass

def crawl(url):
    try:
        response = request(url)
        href_links = get_links(response)
        for link in href_links:
            i = i + 1
            print(i)
            if "#" in link:
                link = link.split("#")[0]
            if url in link and link not in target_links and ".css" not in link and ".ico" not in link:
                target_links.append(link)
                print(link)
                crawl(link)
                print(len(target_links))
    except TypeError:
        pass

target_links = []
url = "gitam.edu"
crawl(url)