#!/usr/bin/env python

import scapy.all as scapy
import optparse

def scan(ip):
    arp_request = scapy.ARP(pdst = ip)
    broadcast = scapy.Ether(dst = "ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout = 1, verbose = False)[0]
    client_list = []
    for element in answered_list:
        print(element[1].summary())
        client_dict = {"ip" : element[1].psrc, "mac" : element[1].hwsrc}
        client_list.append(client_dict)
    return client_list

def print_result(result_list):
    print("IP address\t\t\tMAC address\n-------------------------------------------------")
    for client in result_list:
        print(client["ip"] + "\t\t\t" + client["mac"])

parse = optparse.OptionParser()
parse.add_option("-r", "--range", dest = "ip", help = "Range or IP address to scan")
(options, arguments) = parse.parse_args()
scan_result = scan(options.ip)
print_result(scan_result)