#!/usr/bin/env python

import optparse
import re
import subprocess

def change_mac(interface, new_mac):
    print("[+] Changing MAC to " + new_mac)
    subprocess.call(["ifconfig", interface, "down"])
    subprocess.call(["ifconfig", interface, "hw", "ether", new_mac])
    subprocess.call(["ifconfig", interface, "up"])

def parse_input():
    parse = optparse.OptionParser()
    parse.add_option("-i", "--interface", dest="interface", help="Interface to change MAC address")
    parse.add_option("-m", "--mac", dest="new_mac", help="new MAC address")
    (options, arguments) = parse.parse_args()
    if not options.interface:
        print("[-] Please specify an interface, use --help to know more.")
    elif not options.new_mac:
        print("[-] Please specify a MAC address, use --help to know more.")
    return options

def check_current_mac(interface):
    ifconfig_output_result = subprocess.check_output(["ifconfig", interface])
    mac_address_result = re.search("\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", ifconfig_output_result)
    if mac_address_result:
        return mac_address_result.group(0)
    else:
        print("[-] MAC address could not found")

options = parse_input()

current_mac = check_current_mac(options.interface)
print("[+] Current MAC address = " + current_mac)

change_mac(options.interface, options.new_mac)

current_mac = check_current_mac(options.interface)

if current_mac == options.new_mac:
    print("[+] MAC address changed successfully to " + options.new_mac)
else:
    print("[-] MAC address not changed")

