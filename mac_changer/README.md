Usage: mac_changer.py [options]

Options:
  -h, --help            show this help message and exit
  -i INTERFACE, --interface=INTERFACE
                        Interface to change MAC address
  -m NEW_MAC, --mac=NEW_MAC
                        New MAC address
