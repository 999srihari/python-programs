#!/usr/bin/env python

import subprocess, os

def untar(number):
    for number in range(number, 0, -1):
        file_name = str(number) + ".tar"
        subprocess.call("tar -xvf " + file_name, shell=True)
        with open("filler.txt", "r") as file:
            file_data = file.read()
            with open("data.txt", "a") as data:
                data.write(file_data)
        os.remove(file_name)

untar(1000)